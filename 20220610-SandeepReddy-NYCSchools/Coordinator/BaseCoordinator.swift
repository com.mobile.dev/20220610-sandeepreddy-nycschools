//
//  BaseCoordinator.swift
//  20220610-SandeepReddy-NYCSchools
//
//  Created by Sandeep Reddy Areddy on 6/10/22.
//

import UIKit

protocol Coordinator {
    var navigationController: UINavigationController? { get set }
    func start()
}

class BaseCoordinator: Coordinator {
    
    weak var navigationController: UINavigationController?

    init(navigationController: UINavigationController?) {
        self.navigationController = navigationController
    }

    func start() {
        let coordinator = SchoolListCoordinator(navigationController: navigationController)
        coordinator.start()
    }
    
}
