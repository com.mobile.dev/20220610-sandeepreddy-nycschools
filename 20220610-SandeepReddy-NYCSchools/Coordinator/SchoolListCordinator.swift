//
//  SchoolListCordinator.swift
//  20220610-SandeepReddy-NYCSchools
//
//  Created by Sandeep Reddy Areddy on 6/10/22.
//

import Foundation

/// Coordinator for the NYCSchoolDasboardViewController
///
class SchoolListCoordinator: BaseCoordinator {
    
    var passThroughData: [NYCSchool]?

    override func start() {
        let viewModel = SchoolListViewModel()
        let vc = SchoolListViewController.loadVCFromStoryBoard()
        vc.setViewModel(viewModel: viewModel)
        vc.coordinator = self
        navigationController?.pushViewController(vc, animated: false)
    }
    
    /// <#Description#>
    /// - Parameter index: <#index description#>
    func coordianteToDetails(index:Int?) {
        if let index = index {
            let coordinator = SchoolDetailCooridnator(navigationController: navigationController)
            coordinator.start(passThoughData: passThroughData?[index])
        }
    }
}
