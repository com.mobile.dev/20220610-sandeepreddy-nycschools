//
//  SchoolDetailCoordinator.swift
//  20220610-SandeepReddy-NYCSchools
//
//  Created by Sandeep Reddy Areddy on 6/10/22.
//

import Foundation

/// Coordinator for the NYCSchoolDetailVC
///

class SchoolDetailCooridnator: BaseCoordinator {
    func start(passThoughData:NYCSchool?) {
        let viewModel = SchoolDetailViewModel(passThroughData: passThoughData)
        let vc = DetailsViewController.loadVCFromStoryBoard()
        vc.setViewModel(viewModel: viewModel)
        vc.coordinator = self
        navigationController?.pushViewController(vc, animated: true)
    }
}
