//
//  SchoolDescriptionCell.swift
//  20220610-SandeepReddy-NYCSchools
//
//  Created by Sandeep Reddy Areddy on 6/11/22.
//


import UIKit

class SchoolDescriptionCell: UITableViewCell {

    @IBOutlet weak var labelDescription: UILabel!
    
    ///   setViewModel for the NYCSchoolModel description
    /// - Parameter index: viewModel of the  SchoolDetailViewModel
    func setViewModel(viewModel: SchoolDetailViewModel? , index: Int) {
        labelDescription.text = viewModel?.getDetailModel(index: index)
    }
     
}
