//
//  SchoolScoreCell.swift
//  20220610-SandeepReddy-NYCSchools
//
//  Created by Sandeep Reddy Areddy on 6/11/22.
//


import UIKit

class SchoolScoreCell: UITableViewCell {
    
    @IBOutlet weak var labelMaths: UILabel!
    @IBOutlet weak var labelWriting: UILabel!
    @IBOutlet weak var labelReading: UILabel!
    
    ///   Set the scores of the school
    /// - Parameter index: viewModel of the  SchoolDetailViewModel 
    func setViewModel(viewModel: SchoolDetailViewModel?) {
        labelMaths.text = viewModel?.mathsScore
        labelWriting.text = viewModel?.writingScore
        labelReading.text = viewModel?.readingScore
    }
    
}
