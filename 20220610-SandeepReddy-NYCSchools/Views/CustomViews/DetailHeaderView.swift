//
//  DetailHeaderView.swift
//  20220610-SandeepReddy-NYCSchools
//
//  Created by Sandeep Reddy Areddy on 6/11/22.
//


import UIKit

class DetailHeaderView: UITableViewHeaderFooterView {
    
    @IBOutlet weak var labelSectionTitle: UILabel!
    
    ///   Set the header section title
    /// - Parameter index: viewModel of the  SchoolDetailViewModel , index is the Indexpath.section
    func setViewModel(viewModel:SchoolDetailViewModel? , index: Int?) {
        labelSectionTitle.text = viewModel?.getSectionHeaderTitle(index: index)
    }


}
