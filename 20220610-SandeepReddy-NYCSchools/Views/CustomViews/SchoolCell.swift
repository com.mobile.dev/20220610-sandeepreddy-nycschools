//
//  SchoolCell.swift
//  20220610-SandeepReddy-NYCSchools
//
//  Created by Sandeep Reddy Areddy on 6/11/22.
//

import UIKit


protocol SchoolCellDelegate: AnyObject {
    func makeCall(phoneNumber: String?)
    func composeMail(email: String?)
}


class SchoolCell: UITableViewCell {
    
    @IBOutlet weak var labelSchoolLocation: UILabel!
    @IBOutlet weak var labelSchoolName: UILabel!
    @IBOutlet weak var buttonPhoneNumber: UIButton!
    @IBOutlet weak var buttonEmail: UIButton!
    @IBOutlet weak var labelStudentsCount: UILabel!
    
    weak var delegate: SchoolCellDelegate?
    var viewModel: SchoolListViewModel?
    var detailViewModel: SchoolDetailViewModel?
    var index: Int?
    
    
    ///   setViewModel for the NYCSchoolModel
    /// - Parameter index: viewModel of the  SchoolListViewModel , index of the Indexpath row ,
    func setViewModel (viewModel: SchoolListViewModel?, index: Int?) {
        self.index = index
        self.viewModel = viewModel
        labelSchoolName.text = self.viewModel?.getSchoolName(index: index)
        labelSchoolLocation.text = self.viewModel?.getSchoolLocation(index: index)
        labelStudentsCount.text = self.viewModel?.getTotalStudents(index: index)
        buttonPhoneNumber.isHidden = !(self.viewModel?.isPhoneNumberAvailable(index: index) ?? true)
    }
    
    ///   setDetailViewModel for the NYCSchoolModel
    /// - Parameter index: viewModel of the  SchoolDetailViewModel
    func setDetailViewModel (viewModel: SchoolDetailViewModel?) {
        self.detailViewModel = viewModel
        labelSchoolName.text = self.detailViewModel?.schoolName
        labelSchoolLocation.text = self.detailViewModel?.location
        labelStudentsCount.text = self.detailViewModel?.totalStudents
        buttonPhoneNumber.isHidden = !(self.detailViewModel?.isPhoneNumberAvailable ?? true)
    }
    
 
    @IBAction func didTapPhoneNumber(_ sender: UIButton) {
        if let index = index , let viewModel = viewModel {
            self.delegate?.makeCall(phoneNumber: viewModel.getPhoneNumber(index: index))
        }
        else if let detailViewModel = detailViewModel {
            self.delegate?.makeCall(phoneNumber: detailViewModel.phoneNumber)
        }
    }
    
    @IBAction func didTapEmail(_ sender: UIButton) {
        if let index = index , let viewModel = viewModel {
            self.delegate?.composeMail(email: viewModel.getEmailAddress(index: index))
        }
        else if let detailViewModel = detailViewModel {
            self.delegate?.composeMail(email: detailViewModel.email)
        }
    }
    
}
