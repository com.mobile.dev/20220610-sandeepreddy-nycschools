//
//  APIHelper.swift
//  20220610-SandeepReddy-NYCSchools
//
//  Created by Sandeep Reddy Areddy on 6/11/22.
//

import Foundation

enum URLPath: String {
    case getList = "/s3k6-pzi2.json"
    case getDetails = "/f9bf-2cp4.json"
}

/// Helper class for communicating with server for API calls
class APIHelper {
    static let baseURL = "https://data.cityofnewyork.us/resource"
    
    
    static func getURL(for path:URLPath) -> String {
        return baseURL + path.rawValue
    }
    
    /// Get API call, common use for every API
    /// - Parameters:
    ///   - path: URL end point to be called
    ///   - completion: Clouser to send response data & error details back to the view model
    static func getData(path:URLPath,  completion: @escaping(_ data:Data?, _ error:Error?) -> Void) {
        guard let serviceUrl = URL(string: getURL(for: path)) else { return }
        var request = URLRequest(url: serviceUrl)
        request.httpMethod = "GET"
        
        URLSession.shared.dataTask(with:request) { (data, response, error) in
                completion(data,error)
        }.resume()
    }
    
    /// Get API call, common use for every API
    /// - Parameters:
    ///   - path: URL end point to be called
    ///   - completion: Clouser to send response data & error details back to the view model
    static func postData(path:URLPath, body:[String: Any], completion: @escaping(_ data:Data?, _ error:Error?) -> Void) {
        
    }
    
}
