//
//  SchoolListViewModel.swift
//  20220610-SandeepReddy-NYCSchools
//
//  Created by Sandeep Reddy Areddy on 6/11/22.
//

import Foundation

class SchoolListViewModel: BaseViewModel {
    
    
    var schoolModels: [NYCSchool]?
    var getNumberOfRows: Int? {
        return schoolModels?.count
    }

    
    /// Function to fetch the school list from the server
    func fetchSchoolList(callback: @escaping () -> Void) {
        APIHelper.getData(path: .getList) { data, error in
            if let data = data , let response = try? JSONDecoder().decode([NYCSchool].self, from: data) {
//                self.numberOfSections = 1
                self.schoolModels = response
                callback()
            } else {
                self.showError?(error?.localizedDescription ?? "Unknown error occured")
            }
        }
    }
    
    ///   GetSchoolName
    /// - Parameter index: index of the [NYCSchool]
    /// - Returns: School name of the school
    func getSchoolName(index: Int?) -> String? {
        guard let index = index else {
            return ""
        }
        return schoolModels?[index].schoolName ?? ""
    }
    
    ///   GetSchoolLocation
    /// - Parameter index: index of the [NYCSchool]
    /// - Returns: location of the school combination of address, city , zip
      func getSchoolLocation(index: Int?) -> String? {
        guard let index = index else {
            return ""
        }
        let school = schoolModels?[index]
        var string = ""
        if let address = school?.address {
            string.append(address)
            string.append(", ")
        }
        if let city = school?.city {
            string.append(city)
            string.append(", ")
        }
        if let zip = school?.zip {
            string.append(zip)
        }
        return string
    }
    
    ///   Checking the phone number is available or not
    /// - Parameter index: index of the [NYCSchool]
    /// - Returns: true if phone number exists, false otherwise
    func isPhoneNumberAvailable(index: Int?) -> Bool? {
        return !(getPhoneNumber(index: index)?.isEmpty ?? false)
    }
    
    
    /// - Parameter index: index of the [NYCSchool]
    /// - Returns: phonenumber for school
    func getPhoneNumber(index: Int?) -> String? {
        guard let index = index else {
            return ""
        }
        let school = schoolModels?[index]
        return school?.phoneNumber ?? ""
    }
    
    /// - Parameter index: index of the [NYCSchool]
    /// - Returns: email address of school
    func getEmailAddress(index: Int?) -> String? {
        guard let index = index else {
            return ""
        }
        let school = schoolModels?[index]
        return school?.email ?? ""
    }
    
    /// - Parameter index: index of the NYCSchool
    /// - Returns: total students for school
    func getTotalStudents(index: Int?) -> String? {
        guard let index = index else {
            return ""
        }
        var string = ""
        if let studentCount = schoolModels?[index].totalStudents {
            string =  "Total Students: \(studentCount)"
        }
        return string
    }
    
}
