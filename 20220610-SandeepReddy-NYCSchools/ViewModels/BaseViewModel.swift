//
//  BaseViewModel.swift
//  20220610-SandeepReddy-NYCSchools
//
//  Created by Sandeep Reddy Areddy on 6/11/22.
//

import UIKit


class BaseViewModel {
    /// Throwing  the Error to the View after the service call
    var showError: ((String) -> Void)?
}
    
