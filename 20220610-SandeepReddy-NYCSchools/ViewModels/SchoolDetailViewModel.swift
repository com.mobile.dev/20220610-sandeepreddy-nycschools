//
//  SchoolDetailViewModel.swift
//  20220610-SandeepReddy-NYCSchools
//
//  Created by Sandeep Reddy Areddy on 6/11/22.
//

import Foundation

enum SchoolDetailSections : String {
    case schoolInformation = "Information"
    case detail = "About"
    case scores = "Scores"
    case sports = "Sports"
    case extraCuricularActivities = "ExtraCuricularActivities"
    case languages = "Languages"
}

class SchoolDetailViewModel: BaseViewModel {
    
    var schoolsDetailModels : [NYCSchoolDetail]?
    var schoolModel: NYCSchool?
    var sections: [SchoolDetailSections] = []
    
    /// Initiate the SchoolDetailViewModel with the selected school model
    /// - Parameter passThroughData: set the Selected School model
    init(passThroughData:NYCSchool?) {
        self.schoolModel = passThroughData
    }
    
    
    /// Function to fetch the school list from the server
    func fetchSchoolsDetails(callback: @escaping () -> Void) {
        
        APIHelper.getData(path: .getDetails) { data, error in
            if let data = data , let response = try? JSONDecoder().decode([NYCSchoolDetail].self, from: data){
                self.schoolsDetailModels = response
                self.loadSections()
                callback()
            }
            else {
                self.showError?(error?.localizedDescription ?? "Unknown error occured")
            }
        }
   
    }
    
    
    /// Load the sections based on the Availability
    func loadSections() {
        sections.append(.schoolInformation)
        if !description.isEmpty {
            sections.append(.detail)
        }
        if !(mathsScore.isEmpty && readingScore.isEmpty && writingScore.isEmpty) {
            sections.append(SchoolDetailSections.scores)
        }
        if !extraCuricularActivities.isEmpty {
            sections.append(.extraCuricularActivities)
        }
        if !languages.isEmpty {
            sections.append(.languages)
        }
        if !sports.isEmpty {
            sections.append(.sports)
        }
    }
    
    /// return the number of sections
    var getNumberOfSections: Int? {
        return sections.count 
    }
    
    /// return the number of rows, displaying one for each section
    var getNumberOfRows: Int? {
        return 1
    }
    
    /// Return the phoneNumber
    var phoneNumber: String {
        return schoolModel?.phoneNumber ?? ""
    }
    
    /// Return the phoneNumber
    var email: String {
        return schoolModel?.email ?? ""
    }
    
    /// Return the TotalStudents
    var totalStudents : String {
        var string = ""
        if let studentCount = schoolModel?.totalStudents {
            string =  "Total Students: \(studentCount)"
        }
        return string
    }
    
    /// Return the School name
    var schoolName : String {
        return schoolModel?.schoolName ?? ""
    }
    
    /// Check  the PhoneNumber availabity  of the school
    var isPhoneNumberAvailable: Bool {
        return !phoneNumber.isEmpty
    }
    
    ///   Get the formatted address of the school
    /// - Returns: string containing address, city , zip
    var location : String {
        let school = schoolModel
        var string = ""
        if let address = school?.address {
            string.append(address)
            string.append(", ")
        }
        if let city = school?.city {
            string.append(city)
            string.append(", ")
        }
        if let zip = school?.zip {
            string.append(zip)
        }
        return string
    }
    
    /// - Returns: short description of  the school
    var description : String {
        return schoolModel?.schoolInformation ?? ""
    }
    
    /// - Returns: languages of the school
    var languages: String {
        return schoolModel?.languages ?? ""
    }
    
    /// - Returns: available Sports in the school
    var sports: String {
        return schoolModel?.schoolSports ?? ""
    }
    
    ///   ExtraCuricular Activities available in the school
    /// - Returns: ExtraCuricular Activities of the school
    var extraCuricularActivities: String {
        return schoolModel?.extracurricularActivities ?? ""
    }
    
    
    /// Get the Section header title
    /// - Parameter index: index of the section
    /// - Returns:  Get the Section header title based on the [SchoolDetailSections]
    func getSectionHeaderTitle(index : Int?) -> String {
        guard let index = index else {
            return  ""
        }
        return sections[index].rawValue
    }
    
    /// - Returns: Average Maths score
    var mathsScore: String {
        var string = ""
        if let mathsScore = schoolsDetailModels?.filter({$0.id == schoolModel?.id}).first?.avgMathsScore {
            string = "Maths Score: \(mathsScore)"
        }
        return string
    }
    
    /// - Returns: Average Reading score
    var readingScore:  String {
        var string = ""
        if let readingScore = schoolsDetailModels?.filter({$0.id == schoolModel?.id}).first?.avgReadingScore {
            string = "Reading Score: \(readingScore)"
        }
        return string
    }
    
    /// - Returns: Average Writing score
    var writingScore: String {
        var string = ""
        if let readingScore = schoolsDetailModels?.filter({$0.id == schoolModel?.id}).first?.avgWritingScore {
            string = "Writing Score: \(readingScore)"
        }
        return string
    }
    
    
    /// Section Information based on the section
    /// - Parameter index: index of the section
    /// - Returns:  respective section information
    func getDetailModel(index: Int) -> String {
        let section = sections[index]
        var string = ""
        switch section {
        case .detail:
            string = description
            break
        case .languages:
            string = languages
            break
        case .sports:
            string = sports
            break
        case .extraCuricularActivities:
            string = extraCuricularActivities
            break
        default:
            string = ""
        }
        return string
    }
    
}
