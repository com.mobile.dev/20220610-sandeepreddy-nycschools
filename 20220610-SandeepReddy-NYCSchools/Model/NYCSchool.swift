//
//  NYCSchool.swift
//  20220610-SandeepReddy-NYCSchools
//
//  Created by Sandeep Reddy Areddy on 6/10/22.
//

import Foundation

struct NYCSchool: Decodable {
    
    var schoolName: String?
    var schoolInformation: String?
    var location: String?
    var city: String?
    var zip: String?
    var languages: String?
    var phoneNumber: String?
    var email: String?
    var address : String?
    var stateCode : String?
    var extracurricularActivities : String?
    var totalStudents : String?
    var schoolSports : String?
    var id : String?

    enum CodingKeys: String, CodingKey {
        case schoolName = "school_name"
        case id = "dbn"
        case schoolInformation = "overview_paragraph"
        case languages = "language_classes"
        case phoneNumber = "phone_number"
        case email = "school_email"
        case address = "primary_address_line_1"
        case stateCode = "state_code"
        case extracurricularActivities = "extracurricular_activities"
        case totalStudents = "total_students"
        case schoolSports = "school_sports"
        case location
        case zip
        case city
    }
}

