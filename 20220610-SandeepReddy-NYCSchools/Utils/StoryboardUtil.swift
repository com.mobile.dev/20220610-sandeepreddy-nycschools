//
//  StoryboardUtil.swift
//  20220610-SandeepReddy-NYCSchools
//
//  Created by Sandeep Reddy Areddy on 6/11/22.
//


import UIKit

struct Constants {
     static let schoolCellIndentifier = "SchoolCell"
     static let schoolDescriptionCellIndentifier = "SchoolDescriptionCell"
     static let schoolCellScoreIndentifier = "SchoolScoreCell"
     static let schoolDescriptionHeaderIndentifier = "DetailHeaderView"
     static let schoolDescriptionHeaderHeight = 35.0
     static let phoneNumberInvalidError = "Operation cannot be performed"
     static let emailInvalidError = "Operation cannot be performed"
     static let schoolDashboardNavigationTitle =  "NYC Schools"
     static let schoolDetailNavigationTitle = "Details"
     static let errorViewNibName = "ErrorView"
     static let errorHeading = "Error"
     static let ok = "Ok"
 }


extension UIStoryboard {
    class func main() -> UIStoryboard {
        return UIStoryboard.init(name: "Main", bundle: nil)
    }
}

extension UIViewController {
    static func loadVCFromStoryBoard() -> Self {
        func instantiateFromStoryBoard<T: UIViewController>() -> T {
            return UIStoryboard.main().instantiateViewController(withIdentifier: String(describing: T.self)) as? T ?? UIViewController() as! T
        }
        return instantiateFromStoryBoard()
    }
}
