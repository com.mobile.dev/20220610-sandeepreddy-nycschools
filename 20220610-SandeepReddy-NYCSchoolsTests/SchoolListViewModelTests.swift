//
//  SchoolListViewModelTests.swift
//  20220610-SandeepReddy-NYCSchoolsTests
//
//  Created by Sandeep Reddy Areddy on 6/11/22.
//

import XCTest
@testable import _0220610_SandeepReddy_NYCSchools

class SchoolListViewModelTests: XCTestCase {

    var viewModel: SchoolListViewModel!
    let url = URL(string: "https://data.cityofnewyork.us/resource/s3k6-pzi2.json")!

    
    override func setUp() {
        viewModel = SchoolListViewModel()
                
        let exp = expectation(description: "Loading school list")
        APIHelper.getData(path: .getList) { data, error in
            self.viewModel.schoolModels = try? JSONDecoder().decode([NYCSchool].self, from: data!)
            exp.fulfill()
        }
        
        wait(for: [exp], timeout: 5)
    }
    
    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testListAPI() async throws {
      
        // Use an asynchronous function to download the data.
        let dataAndResponse: (data: Data, response: URLResponse) = try await URLSession.shared.data(from: url, delegate: nil)
        
        // Assert that the actual response matches the expected response.
        let httpResponse = try XCTUnwrap(dataAndResponse.response as? HTTPURLResponse, "Expected an HTTPURLResponse.")
      
        
        XCTAssertEqual(httpResponse.statusCode, 200, "Expected a 200 OK response.")
    }

    func testSchoolName() async {
        XCTAssertNotEqual(self.viewModel.getSchoolName(index: 0), "", "School name is missing")
    }
    
    func testLocation(){
        XCTAssertNotEqual(self.viewModel.getSchoolLocation(index: 0), "", "Location data missing for the school \(self.viewModel.getSchoolName(index: 0) ?? "")")
    }
    
    func testTotalStudents(){
        XCTAssertNotEqual(self.viewModel.getTotalStudents(index: 0), "", "Students data missing for the school \(self.viewModel.getSchoolName(index: 0) ?? "")")
    }
    
    func testPhoneNumberAvailable() {
        XCTAssertNotEqual(self.viewModel.isPhoneNumberAvailable(index: 0), false, "Phone Number missing for the school \(self.viewModel.getSchoolName(index: 0) ?? "")")
    }
    
    func testPhoneNumber() {
        XCTAssertNotEqual(self.viewModel.getPhoneNumber(index: 0), "", "Phone Number missing for the school \(self.viewModel.getSchoolName(index: 0) ?? "")")
    }

}
